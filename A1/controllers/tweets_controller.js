
var route = function(query, response, JSONobj) {
    var JSONresp;
    if (query.length == 1) { // return all tweets [/tweets]
        JSONresp = get_tweets(JSONobj);
    }
    else if (query.length == 2) { // return specific tweet by ID [/tweets/ID]
        JSONresp = get_tweet(query[1], JSONobj);
    }
    
    response.setHeader("Content-Type", "application/json");
    response.statusCode = 200;
    response.end(JSON.stringify(JSONresp));
}

//get list of tweets in favs.json
var get_tweets = function(JSONobj){
    var obj = [];
    for (var i=0; i < JSONobj.length; i++) {
        var t = {
            'id': JSONobj[i].id_str,
            'profile_image_url': JSONobj[i].user.profile_image_url,
            'name': JSONobj[i].user.name,
            'screen_name': JSONobj[i].user.screen_name,
            'text': JSONobj[i].text,
            'created_at': JSONobj[i].created_at,
            'source': JSONobj[i].source
        };
        obj.push(t);
    }
    return obj;
};

//get specific tweet based on id
var get_tweet = function(tweet_id, JSONobj){
    var obj = [];
    for (var i = 0, len = JSONobj.length; i < len; ++i) {
        if (tweet_id == JSONobj[i].id_str){
            var t = {
                'id': JSONobj[i].id_str,
                'profile_image_url': JSONobj[i].user.profile_image_url,
                'name': JSONobj[i].user.name,
                'screen_name': JSONobj[i].user.screen_name,
                'text': JSONobj[i].text,
                'created_at': JSONobj[i].created_at,
                'source': JSONobj[i].source
            };
            obj.push(t);
        }
    }
    return obj;
};

module.exports.route = route;