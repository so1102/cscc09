
var route = function(query, response, JSONobj) {
    var JSONresp;
    if (query.length == 1) { // return list of all users [/users]
        JSONresp = get_users(JSONobj);
    }
    else if (query.length == 2) { // return user's profile by screen name [/users/screen_name]
        JSONresp = get_user(query[1], JSONobj);
    }
    
    response.setHeader("Content-Type", "application/json");
    response.statusCode = 200;
    response.end(JSON.stringify(JSONresp));
};

var inArray = function(obj, arr) {
    for (var i=0; i < arr.length; i++) {
        if (arr[i].id === obj) return true;
    }
    return false;
};

//get list of users in favs.json
var get_users = function(JSONobj){
    var obj = [];
    for (var i=0; i < JSONobj.length; i++) {
        var t = {
            'id': JSONobj[i].id_str,
            'profile_image_url': JSONobj[i].user.profile_image_url,
            'name': JSONobj[i].user.name,
            'screen_name': JSONobj[i].user.screen_name,
            'text': JSONobj[i].text,
            'created_at': JSONobj[i].created_at,
            'source': JSONobj[i].source
        };
        if(!inArray(t.id, obj)) {
            obj.push(t);
        }
    }
    return obj;
};

//get specific user based on screen_name
var get_user = function(screen_name, JSONobj){
    var obj = [];
    for (var i = 0, len = JSONobj.length; i < len; ++i) {
        if (screen_name == JSONobj[i].user.screen_name){
            var t = {
                'id': JSONobj[i].id_str,
                'profile_image_url': JSONobj[i].user.profile_image_url,
                'name': JSONobj[i].user.name,
                'screen_name': JSONobj[i].user.screen_name,
                'text': JSONobj[i].text,
                'created_at': JSONobj[i].created_at,
                'source': JSONobj[i].source,
                'user_description': JSONobj[i].user.description,
                'user_location': JSONobj[i].user.location,
                'url': JSONobj[i].user.url,
                'tweets': JSONobj[i].user.listed_count,
                'followers': JSONobj[i].user.followers_count,
                'following': JSONobj[i].user.friends_count,
                'bg': JSONobj[i].user.profile_background_image_url,
                'banner': JSONobj[i].user.profile_banner_url
            };
            obj.push(t);
        }
    }
    return obj;
};

module.exports.route = route;
