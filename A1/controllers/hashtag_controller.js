
var route = function(query, response, JSONobj) {
    var JSONresp;
    if (query.length == 1) { // return list of all hashtags [/hashtags]
        JSONresp = get_hashtags(JSONobj);
    }
    else if (query.length == 2) { // return tweets with #title [/hashtags/title]
        JSONresp = get_hashtag(query[1], JSONobj);
    }
    
    response.setHeader("Content-Type", "application/json");
    response.statusCode = 200;
    response.end(JSON.stringify(JSONresp));
};

var inArray = function(obj, arr) {
    for (var i=0; i < arr.length; i++) {
        if (arr[i].id === obj) return true;
    }
    return false;
};

var check_hashtag = function(obj) {
    if (obj.hashtags[0] === undefined){
        return false;
    }
    return true;
};

//get list of tweets with hashtags in favs.json
var get_hashtags = function(JSONobj){
    var obj = [];
    for (var i=0; i < JSONobj.length; i++) {
        if (check_hashtag(JSONobj[i].entities)){
            var t = {
                'id': JSONobj[i].id_str,
                'profile_image_url': JSONobj[i].user.profile_image_url,
                'name': JSONobj[i].user.name,
                'screen_name': JSONobj[i].user.screen_name,
                'text': JSONobj[i].text,
                'created_at': JSONobj[i].created_at,
                'via': JSONobj[i].source,
                'location': JSONobj[i].user.location,
                'description': JSONobj[i].user.description,
                'hashtag' : JSONobj[i].entities.hashtags
            }
            if(!inArray(t.hashtag, obj)) {
                obj.push(t);
            }
        }
    }
    return obj;
};

//get specific tweets based on hashtag
var get_hashtag = function(title, JSONobj){
    var obj = [];
    for (var i = 0, len = JSONobj.length; i < len; ++i) {
        if (check_hashtag(JSONobj[i].entities)){
            for (var j = 0; j < JSONobj[i].entities.hashtags.length; j++){
                if (title == JSONobj[i].entities.hashtags[j].text){  
                    var t = {
                        'id': JSONobj[i].id_str,
                        'profile_image_url': JSONobj[i].user.profile_image_url,
                        'name': JSONobj[i].user.name,
                        'screen_name': JSONobj[i].user.screen_name,
                        'text': JSONobj[i].text,
                        'created_at': JSONobj[i].created_at,
                        'via': JSONobj[i].source,
                        'location': JSONobj[i].user.location,
                        'description': JSONobj[i].user.description,
                        'hashtag' : JSONobj[i].entities.hashtags[j].text
                    }
                    obj.push(t);
                }
            }
        }
    }
    return obj;
};

module.exports.route = route;