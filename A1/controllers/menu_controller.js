
var route = function(query, response, JSONobj) {
    var JSONresp = get_menu(query[1], JSONobj);
    
    response.setHeader("Content-Type", "application/json");
    response.statusCode = 200;
    response.end(JSON.stringify(JSONresp));
}

var inArray = function(obj, arr) {
    for (var i=0; i < arr.length; i++) {
        if (arr[i].screen_name === obj || arr[i].hashtag === obj ) return true;
    }
    return false;
}

//get list of menu options in favs.json based on query
var get_menu = function(menu, JSONobj){
    var tweet_ids = [],
        user_names = [],
        hashtags = [];
    
    for (var i=0; i < JSONobj.length; i++) {
        tweet_ids.push({'id': JSONobj[i].id_str});
        if(!inArray(JSONobj[i].user.screen_name, user_names)) {
            user_names.push({'screen_name': JSONobj[i].user.screen_name});
        }
        for(var j=0; j < JSONobj[i].entities.hashtags.length; j++) {
            if(!inArray(JSONobj[i].entities.hashtags[j].text, hashtags)) {
                hashtags.push({'hashtag':JSONobj[i].entities.hashtags[j].text});
            }
        }
    }
    //user and link menu essentially the same so we return the same
    if (menu == "tweets") return tweet_ids;
    else if (menu == "hashtags") return hashtags;
    else return user_names;
};

module.exports.route = route;