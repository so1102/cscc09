/**
 * CSCC09H3 - Assignment 1 - My favourite Things
 * Node.js RESTful API | Twitter Clone - MumbleBee
 * Kevin Chan - chanke25 997385752
 * Catalin Tomsa - tomsacat 996195736
 * David Cheng - chengda6 996952196
 * Donald Seo - seodonal 997832494
 */

var server = process.env.IP,
	port = process.env.PORT,
	http = require('http'),
	path = require('path'),
	url = require('url'),
	fs = require('fs'),
	validTypes = {
		".html" : "text/html",
		".json" : "application/json",
		".js"	: "application/javascript", 
		".ico"	: "image/x-icon",
		".css"	: "text/css",
		".txt"	: "text/plain",
		".jpg"	: "image/jpeg",
		".gif"	: "image/gif",
		".png"	: "image/png",
	},
	JSONobj;

var menu = require('./controllers/menu_controller.js'),
    links = require('./controllers/links_controller.js'),
    tweets = require('./controllers/tweets_controller.js'),
    users = require('./controllers/users_controller.js'),
    hashtags = require('./controllers/hashtag_controller.js');

console.log('Server running at ' + server + port + '/');

http.createServer(function(request, response) {
	//Load JSON into variable
	fs.readFile(__dirname + "/assets/js/favs.json", 'utf8', function(err, data){
        if(err){
            console.log('Error: ' + err);
            return;
        }
        else if (!data){
            console.log('File favs.json not found!');
            return;
        }
        else{
            JSONobj = JSON.parse(data);
        }
	});

	var pathname = url.parse(request.url).pathname;
	var filename = request.url;
	var query = request.url.split("/");
	query = query.slice(1, query.length);
	
	if (pathname === "/") {
        filename = "/index.html";
        loadFile(filename, response);
	}
	else if (query[0] === "menu") {
        menu.route(query, response, JSONobj);
	}
	else if (query[0] === "tweets") {
        tweets.route(query, response, JSONobj);
	}
	else if (query[0] === "users") {
        users.route(query, response, JSONobj);
	}
	else if (query[0] === "links") {
        links.route(query, response, JSONobj);
	}
	else if (query[0] === "hashtags") {
        hashtags.route(query, response, JSONobj);
	}	
	else {
        filename = request.url;
        loadFile(filename, response);
	}

}).listen(port);

function loadFile(filename, response) {
	var ext = path.extname(filename);
	var currFile = __dirname + filename;
	if (validTypes[ext]) {
		fs.exists(currFile, function(exists) {
			if(exists) {
				fs.readFile(currFile, function(err, contents) {
                    if(!err) {
                        response.setHeader("Content-Length", contents.length);
                        response.setHeader("Content-Type", validTypes[ext]);
                        response.statusCode = 200;
                        response.end(contents);
                    } else {
                        response.writeHead(500);
                        response.end();
                    }
                });
			} else {
				console.log("File not found: " + currFile);
				response.writeHead(404);
				response.end();
			}
		}); 
	}
	else {
		response.writeHead(302, {
          'Location': '404.html'
        });
        response.end();
	}	
}

