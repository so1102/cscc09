MumbleBee
=========
Twitter clone based on creating a RESTful API for CSCC09H3 - Web Programming

Uses Node.js for backend, jQuery + jQueryUI + Twitter Bootstrap for frontend


Group Members:
============== 
Kevin Hon Man Chan (chanke25, 997385752)

Catalin Tomsa (tomsacat, 996195736)

David Cheng (chengda6, 996952196)

Donald Seo (seodonal, 997832494)


Sample HTTP Requests:
=====================
Get all tweets in archive   : GET URL:PORT/tweets

Get specific tweet          : GET URL:PORT/tweets/id


Get list of all users       : GET URL:PORT/users

Get user's tweets           : GET URL:PORT/users/screen_name


Get list of all links       : GET URL:PORT/links

Get links by user           : GET URL:PORT/links/screen_name


Get all hashtags            : GET URL:PORT/hashtags

Get tweet by hashtag        : GET URL:PORT/hashtags/hashtag





