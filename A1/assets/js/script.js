/*
 * script.js : contains several useful functions for performing AJAX calls
 * to retrieve JSON data from the server, as well as front-end aesthetics
 */

//Pre-load drop-downs based on information retrieved from favs.json
$(document).ready(function() {
    loadJSON('menu/tweets');
    loadJSON('menu/users');
    loadJSON('menu/links');
    loadJSON('menu/hashtags');
    loadJSON('tweets');
});

//Handler for clearing tweets from main container and resetting canvas
$("#clear-btn").click(function() {
    $('body').css("background-image", "");
    $("#tweets_list, #user_profile").hide("blind", 500, function() {
        $("#tweets_list, #user_profile").html('');
    });
    clearBG();
});

//Adding handlers for showing top level information
$("#tweets, #users, #links, #hashtags").click(function(){
    var curr_elem = $(this).attr('id');
    eventHandler(curr_elem);
});

var clearBG = function() {
    $("body").css("background-color: #3a92c8;"
    +"background: -webkit-radial-gradient(circle,#94d2f8,#3a92c8),"
    +"background: -moz-radial-gradient(circle,#94d2f8,#3a92c8),"
    +"background: -ms-radial-gradient(circle,#94d2f8,#3a92c8)");
}

//Main function for handling selections
var eventHandler = function(query) {
    clearBG();
    $('body').css("background-image", "");
    $("#user_profile, #tweets_list").html('');
    $("#user_profile, #tweets_list").hide("blind", 200);
    loadJSON(query);    
};


//Function for parsing tweet created_at date to a more readable format
function parseTwitterDate(text) {
    var dateObj = new Date(Date.parse(text.replace(/( +)/, ' UTC$1')));
    return dateObj.toDateString() + " @ " + dateObj.toLocaleTimeString().replace(/:\d\d /, ' ');
}

//Main AJAX handler for passing information between client and server
var loadJSON = function(query) {
    var routeURL = document.URL + query;                    
    $.ajax({
        type: 'GET',
        url: routeURL,
        dataType: "json",
        success: function(json_data) {
            var queryArr = query.split('/');
            if (queryArr[0] == "users") {
                //if we want user profile info, show tweets
                (queryArr.length > 1) ? showTweets(json_data, true) : showUsers(json_data);
            }
            else if (queryArr[0] == "tweets")   showTweets(json_data, false);
            else if (queryArr[0] == "links")    showLinks(json_data);
            else if (queryArr[0] == "hashtags") showTweets(json_data, false);
            else if (query == "menu/tweets")    loadMenus("tweets-menu", json_data);
            else if (query == "menu/users")     loadMenus("users-menu", json_data);
            else if (query == "menu/links")     loadMenus("links-menu", json_data);
            else if (query == "menu/hashtags")  loadMenus("hashtags-menu", json_data);
            else {
                console.log("Invalid Request " + query);
            }
        },
        error: function(jqXHR, textStatus, errorThrown) {
            console.log(textStatus, errorThrown);
        }
    });
};

//Function used to load each drop-down with relevant information
var loadMenus = function(menu, json_data) {
    $.each(json_data, function(i, item) {
        var query = "", element;
        if(menu == "tweets-menu") {
            query = "tweets/" + item.id;
            element = item.id          
        }
        else if (menu == "users-menu") {
            query = "users/" + item.screen_name;
            element = item.screen_name; 
        }
        else if (menu == "links-menu") {
            query = "links/" + item.screen_name;
            element = item.screen_name;
        }
        else if (menu == "hashtags-menu") {
            query = "hashtags/" + item.hashtag;
            element = "#" + item.hashtag;
        }
        
        $(document.getElementById(menu)).append(
            "<li><a onclick=\"eventHandler('"+query+"')\">" + element + "</a></li>"
        ); 
    });
};

var showUsers = function(json_data) {
    $.each(json_data, function(i, item) {
        $("#tweets_list").append("<hr><table><tr><td style='padding-right: 10px;''>"
            + "<img class='profile_img' src='"+ item.profile_image_url +"'/></td>"
            + "<td style='padding-top: 5px;'>"
            + "<div class='span2'>@"+item.screen_name 
            + "<div class='span4'><b>"+item.name+"</b></div>"
            + "</td></tr></table>");
    });
    $("#tweets_list").show("blind", 500);
};

var showTweets = function(json_data, showProfile) {
    if(showProfile) { //if we want to show selective user, show profile + tweets
        $("#user_profile").append("<hr/><div class='span4'>"
            + "<img class='profile_img' src='"+ json_data[0].profile_image_url.replace("normal", "bigger") +"'/></div>"
            + "<div class='span4'><h2><b>"+json_data[0].name+"</b></h2></div>"
            + "<h4>@"+json_data[0].screen_name + "</h4>"
            + json_data[0].user_description + "<br/>"
            + json_data[0].user_location + " - " + "<a href='" + json_data[0].url + "'>" + json_data[0].url + "</a>"
            + "<div class='span8'><b>Tweets:</b> " + json_data[0].tweets + "\t|\t<b>Followers:</b> " + json_data[0].followers + "\t|\t<b>Following:</b> " + json_data[0].following + "</div>"
        );
        $("#user_banner").css("background", "url('"+json_data[0].banner+"')");
        $("body").css("background-image", "url('"+json_data[0].bg+"')");
    }    
    $.each(json_data, function(i, item) {
        $("#tweets_list").append("<hr><table><tr><td style='padding-right: 10px;''>"
            + "<a onclick=\"eventHandler('users/"+item.screen_name+"')\"><img class='profile_img' src='"+ item.profile_image_url +"'/></a></td>"
            + "<td style='padding-top: 5px;'>"
            + "<div class='span4'><b>"+item.name+"</b><br/>" + item.text + "<br/>"
            + parseTwitterDate(item.created_at) + " via " + item.source
            + "</div></td></tr></table>");
    });       
    $("#user_profile").show("blind", 500);
    $("#tweets_list").show("blind", 500);
};

var showLinks = function(json_data) {
    $.each(json_data, function(i, item) {
        $("#tweets_list").append("<hr><table><tr><td style='padding-right: 10px;''>"
            + "<img  class='profile_img' src='"+ item.profile_image_url +"'/></td>"
            + "<td style='padding-top: 5px;'>"
            + "<div class='span2'>@"+item.screen_name 
            + "<div class='span4'><b>"+item.name+"</b> - "+parseTwitterDate(item.created_at)+"</div>"
            + item.links + "</td></tr></table>");
    });
    $("#tweets_list").show("blind", 500);
};

