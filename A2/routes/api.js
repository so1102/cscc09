var request = require('request')
  , qs = require('querystring')
  , videogami = require('../config/videogami')
  
var data = {
    username: videogami.user,
    token: videogami.api_key
};  
  
exports.getClips = function(req, res) {
    request.get({
        url: videogami.host + "/clips",
        qs: data,
        json: true,
    }, function (error, response, body) {
        var result = [];
        for (var i=0; i < body.clips.length; i++) {
            if(body.clips[i].gifs) {
                var obj = {
                    'description':body.clips[i].description,
                    'clip_id': body.clips[i]._id,
                    'clip_url': body.clips[i].gifs.min,
                    'video_id' : body.clips[i].parent.pID,
                    'video_url' : body.clips[i].formats.webm
                };
                result.push(obj);
            } else {
                console.log("not found");
            }
        }

        res.end(JSON.stringify(result));
    });
};

exports.getVideo = function(req, res) {
    //var mysql = require("mysql");
    request.get({
        url: videogami.host + "/video/" + req.params.id.substr(1, req.params.id.length),
        qs: data,
        json: true,
    }, function (error, response, body) {
        var result = [];
            if(body.video) {
                result = body.video;
                //result.push(mysql.get_votes(body.id));
                //result.push(mysql.get_votes(body.id));
            } else {
                console.log("not found");
            }

        res.end(JSON.stringify(result));
    });    
}

exports.uploadVideo = function(req, res) {
    console.log("UPLOAD");
    console.log(req.body.title);
    console.log(req.body.description);
    console.log(req.body.file);
}