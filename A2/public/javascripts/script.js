
$(document).ready(function() {
    loadClips();
});

$('#player_back_btn').on("click", function(){
    $('#video_player').get(0).currentTime = 0;
    $('#video_player').get(0).pause();
    $('#popup_video_src').attr('src', 'null');
    $('#video_player').get(0).load();
});

var loadClips = function() {
    var routeURL = document.URL + 'api/clips';                    
    $.ajax({
        beforeSend: function() { $.mobile.showPageLoadingMsg(); },
        complete: function() { $.mobile.hidePageLoadingMsg() },
        type: 'GET',
        url: routeURL,
        dataType: "json",
        success: function(json_data) {
            $.each(json_data, function(i, item) {
                $("#video_matrix").append("<div class='ui-block-c'><div class='ui-body'>"+
                "<a id='"+item.video_id+"' href='#popup' class='load_video_link' data-role='button' data-rel='dialog' data-transition='flip'>"+
                "<img src='"+item.clip_url+"' alt='"+item.description+"'>"+
                "</a></div></div>");
            });
        },
        error: function(jqXHR, textStatus, errorThrown) {
            console.log(textStatus, errorThrown);
        }
    }).done(function( data ) {
        $('.load_video_link').each(function(){
            $(this).click(function(){
                loadVideo($(this).attr('id'));
            });
        });
    });
};

var loadVideo = function(video_id) {
    var routeURL = document.URL + 'api/video/:' + video_id;   
    console.log(routeURL);
    $.ajax({
        type: 'GET',
        url: routeURL,
        dataType: "json",
        success: function(json_data) {
            $('#popup_video_src').attr('src', json_data.formats.mp4);
            $('#video_title').html(json_data.title);
            $('#video_description').html(json_data.description);
            $('#video_player').load();
            $('#video_player').get(0).play();
        },
        error: function(jqXHR, textStatus, errorThrown) {
            console.log("ERROR: " + textStatus, errorThrown);
        }
    });            
}

$("#upload_form").on("submit", function(){
    var routeURL = document.URL + 'api/upload/video';
    console.log(routeURL);
    $.ajax({
        url: routeURL,
        type: 'POST',
        dataType: 'json',
        data: $('#upload_form').serialize(),
        success: function(data) {
            console.log(data);
        },
        error: function(jqXHR, textStatus, errorThrown) {
            console.log("ERROR: " + textStatus, errorThrown);
        }
    });    
});


